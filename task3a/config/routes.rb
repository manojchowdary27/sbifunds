Rails.application.routes.draw do
  resources :funds do
    resources :holdings
  end
  root "funds#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
