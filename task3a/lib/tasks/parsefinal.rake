desc 'This is used to parse the given xls file and saves the parsed results into holdings collection'

task :parsefinal do

#hash model for funds collection
@doc = {
		code:"",
 		name:"",
 		date:"",
 		holdings:[{name:"",isin:"",qty:"",value:"",perc:"",rating:"",remarks:""}],
 		t_value:"",
 		t_perc: ""
	  }   

#Database connections & creating object for EXCEL file 
 	client     = Mongo::Client.new('mongodb://127.0.0.1:27017/task3a_development')
    collection = client[:funds]
	workbook   = Spreadsheet.open 'Equity and Debt Open-Ended and Close – Ended Scheme Portfolios - As on 31 May 2017.xls'
	worksheets = workbook.worksheets


#Starting ruby hash generator
	def start  
		ins = @doc[:holdings] 
                @i +=1        #index to move on to the next row in excel sheet
				@j = 0        #index to move on to the next column in excel sheet
				@k=0          #index for holdings array
		
		while((@sheet[@i,@j] <=> "Cash and Bank Balances")!=0) do    #iterate throgh rows until the specified condition satisified(End Of file)
			
			while(@j<=7) do   #iterate through columns in each row (total seven columns)
				
				if @j==0

					#filtering the not valid rows by checking the name field and value field
					if (@sheet[@i,@j].blank?) || ((@sheet[@i,@j] <=> "Total") == 0) || @sheet[@i,3].blank? || ((@sheet[@i,3] <=> "NIL") ==0) || ((@sheet[@i,@j] <=>"Total   ") == 0) 
                        puts "#{@i}skipped the not valid row"
                        @k-=1
						break
					else

						ins[@k] = { id:@k ,name:"", isin:"",qty:"",value:"", perc:"",rating:"",remarks:""}
					end

					#traversing the each excel sheet rows and columns and preparing the holdings collection document

				    ins[@k][:name] = @sheet[@i,@j]	
			
            	elsif @j==1
					ins[@k][:isin] = @sheet[@i,@j]
			
            	elsif @j==2
					ins[@k][:qty] = @sheet[@i,@j]

            	elsif @j==3
					begin
						ins[@k][:value] = @sheet[@i,@j].value
					rescue
						ins[@k][:value] = @sheet[@i,@j]
					end
			
            	elsif @j==4
					ins[@k][:rating] = @sheet[@i,@j]
			
            	elsif @j==5
					ins[@k][:remarks] = @sheet[@i,@j]
			
            	elsif @j==6
					#ins[@k][:industry] = @sheet[@i,@j]
			
            	elsif @j==7
					begin
						ins[@k][:perc] = @sheet[@i,@j].value
					rescue
						ins[@k][:perc] = @sheet[@i,@j]
					end
				
				else
					puts "This is impossible"
				end #if block ends here
				@j+=1 #column iterator index
			end #Inner while block ends here
			@j = 0   #column iterator index
			@i+=1   #row iterator index
			@k+=1	#holdings array index
		end #Outer Block ends here
	end #start method ends here
# ending for ruby hash generator method



@sheetindex = 0  #index variable to iterate through all excel sheets

	while @sheetindex<(worksheets.count-1) do          
		@sheetindex+=1
		@sheet = worksheets[@sheetindex]
		@doc[:code]  = @sheet.name            #setting the sheet code
		@doc[:name]  = @sheet[1,1]            #setting the sheet name
		@doc[:date]  = @sheet[2,1]            #setting the sheet date
		@i = 7                                #first 6 rows are not required or not valid
											  # so starting row iterator index from 7
		start                 #calling the start function to set the holdings collection with  valid rows only
		@doc[:t_value]   =   @sheet[@i+5,3].value      #setting the Grand Total values
		@doc[:t_perc]    =   @sheet[@i+5,7].value    
		if collection.insert_one(@doc)              #inserting the prepared document in funds collection
					puts "Inserted.."
		end
	end
end