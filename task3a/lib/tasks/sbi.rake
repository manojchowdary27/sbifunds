desc 'This rake file can download the sbimf site latest portfolio xls file'
task :sbi do
    agent = Mechanize.new     #creating instance for Mechanize gem
    page = agent.get("https://www.sbimf.com/en-us/portfolios")   #opening the specified url using Mechanize instance
    d = agent.page.parser.xpath("//ul[@class='list-group ppul']/li//a").first #traversing the html file to get the latest Excel sheet download link
    fi = agent.click(d)  #clicking the founded link(latest link)
    puts fi.save(d.text+".xls")   #saving the file in our rails app 
end