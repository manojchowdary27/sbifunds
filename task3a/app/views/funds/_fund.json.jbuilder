json.extract! fund, :id, :code, :name, :date, :t_value, :t_perc, :created_at, :updated_at
json.url fund_url(fund, format: :json)
