json.extract! holding, :id, :name, :isin, :qty, :value, :perc, :rating, :remarks, :created_at, :updated_at
json.url holding_url(holding, format: :json)
