class Fund
  include Mongoid::Document
  field :code, type: String
  field :name, type: String
  field :date, type: String
  field :t_value, type: Float
  field :t_perc, type: Float
  embeds_many :holdings
end
