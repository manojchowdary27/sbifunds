class Holding
  include Mongoid::Document
  field :name, type: String
  field :isin, type: String
  field :qty, type: Float
  field :value, type: Float
  field :perc, type: Float
  field :rating, type: String
  field :remarks, type: String
  embedded_in :fund
end
