class FundsController < ApplicationController
  before_action :set_fund, only: [:show, :edit, :update, :destroy]

  #method to get all the fund records which are in funds colletion
  def index
    @funds = Fund.all
  end

  #method to show the specified fund
  def show
  end

  # method to create a new empty fund
  def new
    @fund = Fund.new
  end

  # method to edit the specified fund
  def edit
  end

  # method to create new fund
  def create
    @fund = Fund.new(fund_params)

    respond_to do |format|
      if @fund.save
        format.html { redirect_to @fund, notice: 'Fund was successfully created.' }
        format.json { render :show, status: :created, location: @fund }
      else
        format.html { render :new }
        format.json { render json: @fund.errors, status: :unprocessable_entity }
      end
    end
  end

  # method to update the specified fund
  def update
    respond_to do |format|
      if @fund.update(fund_params)
        format.html { redirect_to @fund, notice: 'Fund was successfully updated.' }
        format.json { render :show, status: :ok, location: @fund }
      else
        format.html { render :edit }
        format.json { render json: @fund.errors, status: :unprocessable_entity }
      end
    end
  end

  # method to delete the specified fund
  def destroy
    @fund.destroy
    respond_to do |format|
      format.html { redirect_to funds_url, notice: 'Fund was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # method to set the specified fund used by show ,update, edit and  destroy methods
    def set_fund
      @fund = Fund.find(params[:id])
    end

    # Method to whitelist the required params.
    def fund_params
      params.require(:fund).permit(:code, :name, :date, :t_value, :t_perc)
    end
end
