class HoldingsController < ApplicationController
  before_action :set_holding, only: [:show, :edit, :update, :destroy]

  #method to display all the holdings of specified fund
  def index
    @fund = Fund.find(params[:fund_id])
    @holdings = @fund.holdings.all
  end

  #method to show the specified holding in specified fund
  def show
  end

  #method to create an empty holding instance
  def new 
    @holding = Holding.new
  end

 #method to edit the specified holding in specified fund
  def edit
  end

  # method to create a new holding in specified fund
  def create
    @fund = Fund.find( params[:fund_id] )
    @holding = @fund.holdings.new(holding_params)    
      if @holding.save
         redirect_to fund_holdings_path, notice: 'Holding was successfully created.' 
      else
         render :new 
      end
    end

 #method to update the specified holding in specified fund
  def destroy
    @holding.update(holding_params)
    respond_to do |format|
      format.html { redirect_to holdings_url, notice: 'Holding was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

 #method to delete the specified holding in specified fund
  def destroy
    @holding.destroy
    respond_to do |format|
      format.html { redirect_to holdings_url, notice: 'Holding was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Method to set the specified fund and specified holding used by show,edit,update and destroy methods only
    def set_holding
      @fund = Fund.find(params[:fund_id])
      @holding = @fund.Holdings.find(params[:id])
    end

    # Whitelisting the only required parameters
    def holding_params
      params.require(:holding).permit(:name, :isin, :qty, :value, :perc, :rating, :remarks)
    end
end
