# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
    2.3.1
* Please ensure that you have installed the following gems
   **gem 'mongoid' //to connect the mongodb database
   **gem 'mechanize' //to parse and download the excel file from sbimf website
   **gem 'spreadsheet' //to parse the excel file
   **gem 'bootstrap-sass' //Integrating bootstrap to use bootstrap classes

* Database
   **You should have mongodb server running on port 27017 
   **you can check it by clicking the following link http://127.0.0.1:27017/
* Database initialization
    **Intialize and configure mongodb by the following command
     ***>rails g mongoid:config
     ****The above command will create a database named as "task3a_development"
    **Run the following rake task to download the latest portfolio funds from sbimf website
    ***>rake sbi 
    **Run the following rake task to parse and store the data in mongodb database using mongoid gem
    ***>rake parsefinal
    **The above command will create funds collection in "task3a_development" database
*That's it your database is ready

* How to run the test suite
    Start the server by the following command
    >rails s
    Now open http://localhost:3000
